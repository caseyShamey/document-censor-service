import { Injectable } from '@nestjs/common';
import { CensoredResponseObject } from './redaction';

let processedRedactInput: string[] = [];

@Injectable()
export class AppService {
  censorDocument(
    redactTargets: string,
    documentText = '',
  ): CensoredResponseObject {
    let censoredText: string;
    const redaction = 'XXXX';
    const redactTargetsLength = redactTargets.length;

    if (redactTargets[0] === ' ') {
      redactTargets = redactTargets.substring(1);
    }

    if (
      redactTargets[redactTargetsLength - 1] === ' ' ||
      redactTargets[redactTargetsLength - 1] === ','
    ) {
      redactTargets = redactTargets.substring(0, redactTargetsLength - 1);
    }

    redactTargets = redactTargets
      .replace(/[\u2018\u2019]/g, "'")
      .replace(/[\u201C\u201D]/g, '"');

    censoredText = documentText.slice();

    this.parseWordsAndPhrasesToRedact(redactTargets);
    processedRedactInput.forEach((r) => {
      censoredText = censoredText.replaceAll(r, redaction);
    });
    return { censoredText };
  }

  parseWordsAndPhrasesToRedact(redactTargets: string): void {
    const redactSplit = redactTargets.split('');
    const splitLength = redactSplit.length;
    let startIndex = 0;
    let endIndex = 0;
    let firstDoubleQuote = false;
    let firstSingleQuote = false;

    processedRedactInput = [];

    for (let i = 0; i < splitLength; i++) {
      switch (redactSplit[i]) {
        case '"':
          if (!firstSingleQuote) {
            if (!firstDoubleQuote) {
              startIndex = i + 1;
              firstDoubleQuote = true;
            } else {
              const phrase = redactSplit.slice(startIndex, i).join('');
              processedRedactInput.push(phrase);
              firstDoubleQuote = false;
              endIndex = i;
            }
          }
          break;
        case "'":
          if (!firstDoubleQuote) {
            if (!firstSingleQuote) {
              if (
                i !== 0 &&
                (redactSplit[i - 1] === ' ' || redactSplit[i - 1] === ',')
              ) {
                startIndex = i + 1;
                firstSingleQuote = true;
              } else if (i === 0) {
                startIndex = i + 1;
                firstSingleQuote = true;
              }
            } else {
              const phrase = redactSplit.slice(startIndex, i).join('');
              processedRedactInput.push(phrase);
              firstSingleQuote = false;
              endIndex = i;
            }
          }
          break;
        case ' ':
          if (!firstDoubleQuote && !firstSingleQuote && endIndex !== i - 1) {
            const word = redactSplit.slice(startIndex, i).join('');
            processedRedactInput.push(word);
            endIndex = i;
            startIndex = i + 1;
          }
          break;
        case ',':
          if (!firstDoubleQuote && !firstSingleQuote && endIndex !== i - 1) {
            const word = redactSplit.slice(startIndex, i).join('');
            processedRedactInput.push(word);
            endIndex = i;
            startIndex = i + 1;
          }
          break;
      }
      if (i === splitLength - 1) {
        if (
          !firstDoubleQuote &&
          !firstSingleQuote &&
          endIndex !== splitLength - 1
        ) {
          if (startIndex === 0) {
            const word = redactSplit.slice().join('');
            processedRedactInput.push(word);
          } else if (
            redactSplit[endIndex] === '"' ||
            redactSplit[endIndex] === "'"
          ) {
            const word = redactSplit.slice(endIndex + 2).join('');
            processedRedactInput.push(word);
          } else {
            const word = redactSplit.slice(endIndex + 1).join('');
            processedRedactInput.push(word);
          }
        }
      }
    }
  }
}
