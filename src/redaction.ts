export class RedactionObject {
  redactTargets: string;
  documentText: string;
}

export class CensoredResponseObject {
  censoredText: string;
}
