import { Body, Controller, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { CensoredResponseObject, RedactionObject } from './redaction';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post()
  async censorDocument(
    @Body() redaction: RedactionObject,
  ): Promise<CensoredResponseObject> {
    const text = this.appService.censorDocument(
      redaction.redactTargets,
      redaction.documentText,
    );
    return text;
  }
}
